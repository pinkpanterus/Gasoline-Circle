﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GasoineCircle
{
    public class BombMoover_2 : MonoBehaviour
    {
        public Vector3 targetPos;

        public float speed = 75f;

        void Update()
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * speed);

            //transform.LookAt(targetPos, -Vector3.up);
           // transform.Translate(Vector3.forward * speed * Time.deltaTime);
            //transform.LookAt(targetPos, Vector3.up);
        }


        void OnDrawGizmos()
        {
                      // Draw a yellow sphere at the transform's position
                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(targetPos, 2);
           
        }
    }
}
