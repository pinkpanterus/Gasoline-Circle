﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GasoineCircle
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager _instance;
        public static GameManager Instance { get { return _instance; } }

        public enum GameState { MainMenu, Game, Win, Lose };
        public GameState gameState { get; private set; }        

        //Cars
        [SerializeField] private Transform[] carSpawnpoints;
        private Dictionary<int, GameObject> cars = new Dictionary<int, GameObject>();
        public Dictionary<int, GameObject> Cars { get { return cars; } }

        private GameObject[] carsSpawned;
        public GameObject[] CarsSpawned { get { return carsSpawned; } private set { carsSpawned = value; } }

        private String[] playerNames;

        //Tracks
        [SerializeField] private TrackScript[] tracks;
        //[SerializeField] private int[] startTrack;
        [SerializeField] private Path[] paths;
        [SerializeField] private int[] currentTrack;
        [SerializeField] private int[] nextTrack;
        [SerializeField] private int[] lastRightTrack;
        [SerializeField] private int lapsForWin = 10;
        [SerializeField] private bool[] isOnTrack;
        [SerializeField] private bool[] isRightDirection;


        public int[] CurrentTrack { get { return currentTrack;} set { currentTrack = value; /*ControlTrackOrder(); */} }

        //Laps
        [SerializeField] private int[] laps;
        private int[] techLaps;

        //Weapon
        [SerializeField] private int[] ammoCount;
        [SerializeField] private GameObject[] ammoPrefab;
        [SerializeField] private Vector3[] aimPos;

        //Events
        public Action<GameState> OnStateChange;
        public Action<int,int> OnLapsChange;
        public Action<string> OnMessageRaised;
        public Action onCarSpawned;
        public Action onButtonPressed;
        //public Action onGamePaneltEnabled;

        [SerializeField] private string winningPlayerName;
        public string WinningPlayerName { get { return winningPlayerName;} private set { winningPlayerName = value;}}


        private void ControlTrackOrder()
        {
            if (gameState == GameState.Game) 
            {
                for (int i = 0; i < CarsSpawned.Length; i++)
                {
                    //Debug.Log("Changing next track");

                    if (currentTrack[i] != 0 && currentTrack[i] == nextTrack[i])
                    {
                        lastRightTrack[i] = currentTrack[i];

                        if (currentTrack[i] < paths[i].tracksForCar.Max())
                            nextTrack[i] = currentTrack[i] + 1;
                        else
                            nextTrack[i] = paths[i].tracksForCar.Min();
                    }

                    if (currentTrack[i] == 0)
                    {
                        if (isOnTrack[i])
                        {
                            isOnTrack[i] = false;
                            //Debug.Log("Player " + (i + 1) + " return to track");
                            //OnMessageRaised?.Invoke(CarsSpawned[i].GetComponentInChildren<VehicleCustomeInput>().PlayerName + " return to track!");
                            OnMessageRaised?.Invoke(playerNames[i] + " return to track!");
                        }
                    }
                    else
                    {
                        isOnTrack[i] = true;
                    }

                    if (currentTrack[i] != nextTrack[i] && currentTrack[i] != lastRightTrack[i] && currentTrack[i] != 0)
                    {
                        if (isRightDirection[i])
                        {
                            isRightDirection[i] = false;
                            //Debug.Log("Player " + (i + 1) + "! Wrong way! Turn around!");
                            //OnMessageRaised?.Invoke(CarsSpawned[i].GetComponentInChildren<VehicleCustomeInput>().PlayerName + " turn around!");
                            OnMessageRaised?.Invoke(playerNames[i] + " turn around!");
                        }
                    }
                    else
                    {
                        isRightDirection[i] = true;
                    }
                }
            }

            
        }

        public void SetGameState(GameState gameState)
        {
            this.gameState = gameState;
            OnStateChange?.Invoke(gameState);
            CheckGameState(gameState);
            Debug.Log("Game state changed to: " + gameState);
        }

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }           
        }

        private void InitializeCarsAndTracks() 
        {
            LoadCars();
            InitializeTracksAndCars();      
        }

        private void InitializeTracksAndCars() 
        {
            tracks = GameObject.FindObjectsOfType<TrackScript>();
            tracks.OrderBy(t => t.GetComponent<TrackScript>().TrackID).ToArray();

            //startTrack = new int[cars.Count];
            carsSpawned = new GameObject[cars.Count];
            playerNames = new String[cars.Count];
            currentTrack = new int[cars.Count];
            nextTrack = new int[cars.Count];
            lastRightTrack = new int[cars.Count];
            isOnTrack = new bool[cars.Count];
            isRightDirection = new bool[cars.Count];
            laps = new int[cars.Count];
            techLaps = new int[cars.Count];
            ammoCount = new int[cars.Count];
            ammoPrefab = new GameObject[cars.Count];
            aimPos = new Vector3 [cars.Count];
        }
        private void LoadCars()
        {
            var carsLoaded = Resources.LoadAll("Cars", typeof(GameObject)).Cast<GameObject>().ToArray();

            carsLoaded.OrderBy(c => c.GetComponentInChildren<VehicleCustomeInput>().ID);

            foreach (var C in carsLoaded) 
            {
                Debug.Log("Car ID: " + C.GetComponentInChildren<VehicleCustomeInput>().ID);
            }
            
            for (int i = 0; i < carsLoaded.Length; i++)
            {
                cars.Add(i, carsLoaded[i]);                
            }         

        }

        void Start()
        {
            SetGameState(GameState.MainMenu);

            InitializeCarsAndTracks();            
            InitializeFinishLines();
        }

        private void InitializeFinishLines()
        {
            FinishScript[] finishLines = GameObject.FindObjectsOfType<FinishScript>();

            foreach (var f in finishLines) 
            {
                f.onFinishTriggered += CountLaps;
            }
        }

        private void CountLaps(GameObject car)
        {
            var car_id = car.GetComponent<VehicleCustomeInput>().ID;

            techLaps[car_id]++;

            if (techLaps[car_id] > 1 && isRightDirection[car_id])
            {
                laps[car_id]++;
                OnLapsChange?.Invoke(car_id, laps[car_id]);
                OnMessageRaised?.Invoke(car.GetComponentInChildren<VehicleCustomeInput>().PlayerName + " finished lap!");
            }

            if (laps[car_id] >= lapsForWin) 
            {
                WinningPlayerName = car.GetComponent<VehicleCustomeInput>().PlayerName;
                SetGameState(GameState.Win);
            }
        }

        void Update()
        {

        }

        private void CheckGameState(GameState currentGameState)
        {
            switch (currentGameState)
            {
                case GameState.MainMenu:
                    {
                    
                    }                   
                    break;
                case GameState.Game:
                    {                     

                        SpawnCars();
                        InvokeRepeating("ControlTrackOrder", 0.5f, 0.1f);

                        //foreach (var c in cars)
                        //{
                        //    c.Value.GetComponentInChildren<EVP.VehicleAudio>().enabled = true;
                        //}
                    }                   
                    break;
                case GameState.Win:
                    {
                        foreach (var c in GameObject.FindGameObjectsWithTag("Player")) 
                        {
                            Destroy(c.gameObject);
                            //c.Value.GetComponentInChildren<EVP.VehicleAudio>().enabled = false;
                        }
                    }                    
                    break;
                //case GameState.Lose:
                //    //Show Lose panel
                //    break;               
            }
        }

        private void SpawnCars()
        {
            paths = new Path[cars.Count];
            for (int i = 0; i < paths.Length; i++) 
            {
                paths[i].tracksForCar = new int[tracks.Length];
            }
            

            for (int i = 0; i < cars.Count; i++)
            {
                var car = Instantiate(cars[i], carSpawnpoints[i].position, carSpawnpoints[i].rotation);
                car.GetComponentInChildren<VehicleCustomeInput>().onFireButtonPressed += Fire;
                ammoCount[i] = 1;
                ammoPrefab[i] = car.GetComponentInChildren<VehicleCustomeInput>().BombPrefab;

                carsSpawned[i] = car;

                playerNames[i] = CarsSpawned[i].GetComponentInChildren<VehicleCustomeInput>().PlayerName;

                paths[i].car = car.GetComponentInChildren<VehicleCustomeInput>().ID;
               
                paths[i].tracksForCar[0] = carSpawnpoints[i].GetComponentInParent<TrackScript>().TrackID;
                                                
                for (int p = 1; p < tracks.Length; p++) 
                {
                    if (paths[i].tracksForCar[p - 1] < tracks.Length)
                        paths[i].tracksForCar[p] = paths[i].tracksForCar[p - 1] + 1;
                    else
                        paths[i].tracksForCar[p] = 1;
                }

                lastRightTrack[i] = paths[i].tracksForCar[0];
                nextTrack[i] = paths[i].tracksForCar[1];

                isOnTrack[i] = true;
                isRightDirection[i] = true;
            }

            onCarSpawned?.Invoke();
        }

        private void Fire(int car_id)
        {
            Debug.Log("Fire request received for car with id: " + car_id);
            Debug.Log(carsSpawned[car_id].GetComponentInChildren<VehicleCustomeInput>().PlayerName);

            if(ammoCount[car_id] > 0)
            {
                var aims = GameObject.FindObjectsOfType<VehicleCustomeInput>().Where(c => c.ID != car_id).ToArray();

                for (int i = 0; i < aims.Length; i++)
                {
                    Vector3 randomPos = Camera.main.ScreenToWorldPoint(new Vector3(UnityEngine.Random.Range(0, Screen.width), UnityEngine.Random.Range(0, Screen.height), Camera.main.farClipPlane / 2));
                    randomPos.y = Camera.main.transform.position.y + 200;

                    Vector3 targetPos = predictedPosition(aims[i].transform.position, randomPos, aims[i].GetComponentInChildren<Rigidbody>().velocity, ammoPrefab[car_id].GetComponent<BombMoover_2>().speed); // не знаю пока сокрость бомб
                    //targetPos.x += UnityEngine.Random.Range(-offset, offset);
                    //targetPos.z += UnityEngine.Random.Range(-offset, offset);

                    aimPos[i] = targetPos;

                    var bomb = Instantiate(ammoPrefab[car_id], randomPos, Quaternion.Euler(aimPos[i]));
                    //bomb.AddComponent<BombMoover_2>();
                    bomb.GetComponent<BombMoover_2>().targetPos = aimPos[i];
                }

                //GameObject[] aims = carsSpawned.Where(c => c.GetComponentInChildren<VehicleCustomeInput>().ID != car_id).ToArray(); //TODO find aims_targets at car spawn
                //float offset = 0.0001F;


                //for (int i = 0; i < aims.Length; i++)
                //{
                //    Vector3 randomPos = Camera.main.ScreenToWorldPoint(new Vector3(UnityEngine.Random.Range(0, Screen.width), UnityEngine.Random.Range(0, Screen.height), Camera.main.farClipPlane / 2));
                //    randomPos.y = Camera.main.transform.position.y + 100;

                //    Vector3 targetPos = predictedPosition(aims[i].transform.position, randomPos, aims[i].GetComponentInChildren<Rigidbody>().velocity, 5); // не знаю пока сокрость бомб
                //    //targetPos.x += UnityEngine.Random.Range(-offset, offset);
                //    //targetPos.z += UnityEngine.Random.Range(-offset, offset);

                //    aimPos[i] = targetPos;                                      

                //    var bomb = Instantiate(ammoPrefab[car_id], randomPos, Quaternion.Euler(aimPos[i]));
                //    bomb.AddComponent<BombMoover_2>();
                //    bomb.GetComponent<BombMoover_2>().targetPos = aimPos[i];
                //}     

                //for (int i = 0; i < carsSpawned.Length; i++)
                //{
                //    if (i != car_id) 
                //    {
                //        Vector3 randomPos = Camera.main.ScreenToWorldPoint(new Vector3(UnityEngine.Random.Range(0, Screen.width), UnityEngine.Random.Range(0, Screen.height), Camera.main.farClipPlane / 2));
                //        randomPos.y = Camera.main.transform.position.y + 100;

                //        //Vector3 targetPos = predictedPosition(carsSpawned[i].transform.position, randomPos, carsSpawned[i].GetComponentInChildren<Rigidbody>().velocity, 75); // не знаю пока сокрость бомб
                //        //targetPos.x += UnityEngine.Random.Range(-offset, offset);
                //        //targetPos.z += UnityEngine.Random.Range(-offset, offset);

                //        //aimPos[i] = targetPos;

                //        var bomb = Instantiate(ammoPrefab[car_id], randomPos, Quaternion.Euler(aimPos[i]));
                //        bomb.AddComponent<BombMoover_2>();
                //        bomb.GetComponent<BombMoover_2>().targetPos = carsSpawned[i].transform.position;
                //    }

                //}

            }
        }

    

        private Vector3 predictedPosition(Vector3 targetPosition, Vector3 shooterPosition, Vector3 targetVelocity, float projectileSpeed)
        {
            Vector3 displacement = targetPosition - shooterPosition;
            float targetMoveAngle = Vector3.Angle(-displacement, targetVelocity) * Mathf.Deg2Rad;
            //if the target is stopping or if it is impossible for the projectile to catch up with the target (Sine Formula)
            if (targetVelocity.magnitude == 0 || targetVelocity.magnitude > projectileSpeed && Mathf.Sin(targetMoveAngle) / projectileSpeed > Mathf.Cos(targetMoveAngle) / targetVelocity.magnitude)
            {
                Debug.Log("Position prediction is not feasible.");
                return targetPosition;
            }
            //also Sine Formula
            float shootAngle = Mathf.Asin(Mathf.Sin(targetMoveAngle) * targetVelocity.magnitude / projectileSpeed);
            return targetPosition + targetVelocity * displacement.magnitude / Mathf.Sin(Mathf.PI - targetMoveAngle - shootAngle) * Mathf.Sin(shootAngle) / targetVelocity.magnitude;
        }

        public void Play()
        {
            onButtonPressed?.Invoke();
            SetGameState(GameState.Game);
        }

        public void Quit()
        {
            onButtonPressed?.Invoke();
            Application.Quit();
        }

        public void MainMenu()
        {
            onButtonPressed?.Invoke();
            SetGameState(GameState.MainMenu);
        }
    }
}