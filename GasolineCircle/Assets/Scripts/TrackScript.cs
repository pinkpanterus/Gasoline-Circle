﻿using System;
using UnityEngine;

namespace GasoineCircle
{
    public class TrackScript : MonoBehaviour
    {
        [SerializeField] private int track_ID;
        public Action<int, int> onCarEnteredTrack;
        public Action<int, int> onCarExitedTrack;
        public int TrackID { get { return track_ID;}}

        //private void OnCollisionEnter(Collision collisionObj)
        //{
        //    Debug.Log("Collided track #:" + track_ID);

        //    if(collisionObj.gameObject.layer == 9)
        //        onCarEnteredTrack?.Invoke(track_ID, collisionObj.gameObject.GetComponent<VehicleCustomeInput>().ID);
        //}

        //private void OnCollisionExit(Collision collisionObj)
        //{
        //    if (collisionObj.gameObject.layer == 9)
        //        onCarExitedTrack?.Invoke(track_ID, collisionObj.gameObject.GetComponent<VehicleCustomeInput>().ID);
        //}
    }
}
