﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GasoineCircle.GameManager;

namespace GasoineCircle
{
    public class UI_Servise : MonoBehaviour
    {
        [SerializeField] private Text player_1_Title;
        [SerializeField] private Text player_2_Title;

        [SerializeField] private Text player_1_LapsText;
        [SerializeField] private Text player_2_LapsText;
        //[SerializeField] private Text player_3_LapsText;
        //[SerializeField] private Text player_4_LapsText;

        [SerializeField] private GameObject MainMenuPanel;
        [SerializeField] private GameObject GamePanel;
        [SerializeField] private GameObject WinPanel;

        [SerializeField] private AudioClip buttonClickSound;
        [SerializeField] private AudioClip messageSound;
        private AudioSource audiosource; 


        void Start()
        {
            audiosource = GetComponent<AudioSource>();

            GameManager.Instance.OnLapsChange += UpdateLaps;
            GameManager.Instance.OnMessageRaised += ShowMessage;
            GameManager.Instance.OnStateChange += ShowPanel;
            GameManager.Instance.onCarSpawned += SetPlayerNames;
            GameManager.Instance.onCarSpawned += PlayButtonClick;            
        }

        private void StartListenMessageScript()
        {
            Invoke("SubscriseToMessage", 0.5f);
        }

        private void SubscriseToMessage() 
        {
            MessageScript.Instance.onMessageSend += PlayMessageSound;
        }

        private void PlayMessageSound()
        {
            audiosource.PlayOneShot(messageSound);
        }

        private void PlayButtonClick()
        {
            audiosource.PlayOneShot(buttonClickSound);
        }

        private void ShowPanel(GameManager.GameState currentGameState)
        {
            switch (currentGameState)
            {
                case GameManager.GameState.MainMenu:
                    {
                        MainMenuPanel.SetActive(true);
                        GamePanel.SetActive(false);
                        WinPanel.SetActive(false); ;
                    }
                    break;
                case GameManager.GameState.Game:
                    {
                        MainMenuPanel.SetActive(false);
                        GamePanel.SetActive(true);
                        WinPanel.SetActive(false);

                        StartListenMessageScript();
                        //Invoke("SetPlayerNames",0.1f);
                    }
                    break;
                case GameState.Win:
                    {
                        MainMenuPanel.SetActive(false);
                        GamePanel.SetActive(false);
                        WinPanel.SetActive(true);
                        WinPanel.GetComponentInChildren<Text>().text = GameManager.Instance.WinningPlayerName + " win!";
                    }
                    break;
                    //case GameState.Lose:
                    //    //Show Lose panel
                    //    break;               
            }
        }

        private void SetPlayerNames() 
        {
            //Debug.Log("Cars spawned event triggered");

            player_1_Title.GetComponent<Text>().text = GameManager.Instance.CarsSpawned[0].GetComponentInChildren<VehicleCustomeInput>().PlayerName;
            player_2_Title.GetComponent<Text>().text = GameManager.Instance.CarsSpawned[1].GetComponentInChildren<VehicleCustomeInput>().PlayerName;
        }

        private void ShowMessage(string message)
        {
            MessageScript.Instance.PopUp(message);
        }

        private void UpdateLaps(int car_id, int lapsCount)
        {
            switch (car_id) 
            {
                case 0:
                    player_1_LapsText.text = "Laps: " + lapsCount;
                    break;
                case 1:
                    player_2_LapsText.text = "Laps: " + lapsCount;
                    break;
                //case 3:
                //    player_3_LapsText.text = "Laps: " + lapsCount;
                //    break;
                //case 4:
                //    player_4_LapsText.text = "Laps: " + lapsCount;
                //    break;
            }
        }

        void Update()
        {

        }

        private void OnDestroy()
        {
            GameManager.Instance.OnLapsChange -= UpdateLaps;
            GameManager.Instance.OnMessageRaised -= ShowMessage;
            GameManager.Instance.OnStateChange -= ShowPanel;
            GameManager.Instance.onCarSpawned -= SetPlayerNames;
            GameManager.Instance.onCarSpawned -= PlayButtonClick;
        }
    }
}