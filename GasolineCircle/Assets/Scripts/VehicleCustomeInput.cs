﻿using UnityEngine;
using System;

namespace GasoineCircle
{
	public class VehicleCustomeInput : MonoBehaviour
	{
		[Header("Player data")]
		[SerializeField] private Player_SO player_SO;
		[SerializeField] private Transform bombSpawnpoint;
		private GameObject bombPrefab;
        public GameObject BombPrefab { get { return bombPrefab;} private set { bombPrefab = value; } }

        [SerializeField] private int id;
		public int ID { get { return id; } }

		[SerializeField] private string playerName;
		public string PlayerName { get { return playerName; } private set { playerName = value; } }

		public Action<int> onFireButtonPressed;

		[Space(5)]
		public EVP.VehicleController target;
        public bool continuousForwardAndReverse = true;     

        [Space(5)]
		private string steerAxis;
		private string throttleAndBrakeAxis;
		private float steerInput = 0.0f;
		private float forwardInput = 0.0f;
		private float throttleInput = 0.0f;


		//public Action<int, int> onCheckTrack;

		[SerializeField] private GameManager.GameState currentGAmeState; 


		void OnEnable()
		{
			id = player_SO.playerID;
			bombPrefab = player_SO.bombPrefab;
			playerName = player_SO.playerName;

			if (id == 0)
			{
				steerAxis = "Horizontal1";
				throttleAndBrakeAxis = "Vertical1";
			}
			else 
			{
				steerAxis = "Horizontal2";
				throttleAndBrakeAxis = "Vertical2";
			}

			// Cache vehicle
			if (target == null)
				target = GetComponent<EVP.VehicleController>();		
			
		}

        private void Awake()
        {
			
		}

        private void Start()
        {
			currentGAmeState = GameManager.Instance.gameState;
			GameManager.Instance.OnStateChange += SyncGameState;

			//InvokeRepeating("CheckTrack", 0.1f, 0.5f);
		}

		

		//void CheckTrack()
		//{
		//	int layerMask = 1 << 10; // tracks layer mask = 10. Shift the index of the layer (10) to get a bit mask.
		//	RaycastHit hit;
	
		//	// Does the ray intersect any objects excluding the player layer
		//	if (Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.up), out hit, 10, layerMask))
		//	{
		//		onCheckTrack?.Invoke(hit.collider.gameObject.GetComponent<TrackScript>().TrackID, ID);

		//		//Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.up) * hit.distance, Color.yellow);
		//		//Debug.Log(hit.collider.gameObject.GetComponent<TrackScript>().TrackID);
		//	}
		//	else
		//	{
		//		onCheckTrack?.Invoke(0, ID);

		//		//Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.up) * 1000, Color.white);
		//		//Debug.Log("Did not Hit");
		//	}


		//}


		private void SyncGameState(GameManager.GameState game_state)
        {
			currentGAmeState = game_state;
			Debug.Log("Передано состояние: " + currentGAmeState);
		}

        void OnDisable()
		{
			if (target != null)
			{
				target.steerInput = 0.0f;
				target.throttleInput = 0.0f;
				target.brakeInput = 0.0f;
				target.handbrakeInput = 0.0f;
			}
		}


		void Update()
		{
            if (target == null || currentGAmeState != GameManager.GameState.Game) return;

			//if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
			//	FireRequest(id);

			if (id == 1)
			{
				if (Input.GetKeyDown(KeyCode.S))
				{
					FireRequest(id);
					Debug.Log("Fire request from player with ID: " + id);
				}
                   
            }
            else if (id == 0)
            {
				if (Input.GetKeyDown(KeyCode.DownArrow))
				{
					FireRequest(id);
					Debug.Log("Fire request from player with ID: " + id);
				}
                   
            }
        }

        private void FireRequest(int playerID)
        {
			onFireButtonPressed?.Invoke(playerID);
		}

		private void Fire(int ID) 
		{
			if (ID == id) 
			{
				var bomb = Instantiate(Resources.Load("Bombs/" + bombPrefab.name), bombSpawnpoint.position, bombSpawnpoint.rotation);
			}
		}

        void FixedUpdate()
		{

			if (target == null || currentGAmeState != GameManager.GameState.Game) return;

			// Read the user input
			steerInput = Mathf.Clamp(Input.GetAxis(steerAxis), -1.0f, 1.0f);			
            forwardInput = Mathf.Clamp01(Input.GetAxis(throttleAndBrakeAxis));


			if (continuousForwardAndReverse)
			{
				float minSpeed = 0.1f;
				float minInput = 0.1f;

				if (target.speed > minSpeed)
				{
					throttleInput = forwardInput;
					
				}
				else
				{
					if (forwardInput > minInput)
					{
						if (target.speed < -minSpeed)
						{
							throttleInput = 0.0f;
							
						}
						else
						{
							throttleInput = forwardInput;							
						}
					}
                }
			}			

			// Apply input to vehicle
			target.steerInput = steerInput;
			target.throttleInput = throttleInput;			
		}

        private void OnDestroy()
        {
			if(GameManager.Instance)
				GameManager.Instance.OnStateChange -= SyncGameState;
		}
    }
}